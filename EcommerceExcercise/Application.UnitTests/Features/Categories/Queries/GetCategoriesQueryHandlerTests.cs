﻿using Application.Contracts.Logging;
using Application.Contracts.Persistence;
using Application.DTOs.Categories;
using Application.Features.Category.Queries;
using Application.MappingProfiles;
using Application.UnitTests.Mocks;
using AutoMapper;
using Moq;
using Shouldly;

namespace Application.UnitTests.Features.Categories.Queries;


public class GetCategoriesQueryHandlerTests
{
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;
    private IMapper _mapper;
    private Mock<IAppLogger<GetCategoriesQueryHandler>> _mockAppLogger;

    public GetCategoriesQueryHandlerTests()
    {
        _mockUnitOfWork = MockCategoryRepository.GetCategoriesMockCategoryRepositoryAsync();
        var mapperConfig = new MapperConfiguration(c =>
        {
            c.AddProfile<CategoryProfile>();
        });

        _mapper = mapperConfig.CreateMapper();
        _mockAppLogger = new Mock<IAppLogger<GetCategoriesQueryHandler>>();
    }

    [Fact]
    public async Task GetCategoriesTest()
    {
        var handler = new GetCategoriesQueryHandler(_mapper, _mockUnitOfWork.Object, _mockAppLogger.Object);

        var result = await handler.Handle(new GetCategoriesQuery(), CancellationToken.None);

        result.ShouldBeOfType<List<CategoryDto>>();
        result.Count().ShouldBe(3);
    }
}
