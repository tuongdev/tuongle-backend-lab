﻿using Application.Contracts.Persistence;
using Domain;
using Moq;

namespace Application.UnitTests.Mocks;

public class MockCategoryRepository
{
    public static Mock<IUnitOfWork> GetCategoriesMockCategoryRepositoryAsync()
    {
        var categories = new List<Category> {
            new Category
            {
                Id = 1,
                Name = "Test Type A",
                CreateDate = DateTime.Now,
            },
            new Category
            {
                Id = 2,
                Name = "Test Type B",
                CreateDate = DateTime.Now,
            },
            new Category
            {
                Id = 3,
                Name = "Test Type C",
                CreateDate = DateTime.Now,
            },
        };
        
        var mockRepo = new Mock<IUnitOfWork>();

        mockRepo.Setup(r => r.Categories.GetAsync()).ReturnsAsync(categories);

        mockRepo.Setup(r => r.Categories.CreateAsync(It.IsAny<Category>())).Returns((Category category) =>
        {
            categories.Add(category); 
            return Task.FromResult(category); 
        });

        return mockRepo;
    }
}
