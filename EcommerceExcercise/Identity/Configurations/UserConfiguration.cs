﻿using Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.Configurations;

public class UserConfiguration : IEntityTypeConfiguration<ApplicationUser>
{
    public void Configure(EntityTypeBuilder<ApplicationUser> builder)
    {
        var hasher = new PasswordHasher<ApplicationUser>();
        builder.HasData(
             new ApplicationUser
             {
                 Id = "a6f060df-0d3b-446e-a1d2-bbd80a528ad7",
                 FirstName = "System",
                 LastName = "Admin",
                 UserName = "admin",
                 NormalizedUserName = "admin",
                 PasswordHash = hasher.HashPassword(null, "123456"),
                 EmailConfirmed = true
             },
             new ApplicationUser
             {
                 Id = "3055d30b-a83d-4917-984c-a1695237167e",
                 FirstName = "System",
                 LastName = "User",
                 UserName = "user",
                 NormalizedUserName = "user",
                 PasswordHash = hasher.HashPassword(null, "123456"),
                 EmailConfirmed = true
             }
        );
    }
}
