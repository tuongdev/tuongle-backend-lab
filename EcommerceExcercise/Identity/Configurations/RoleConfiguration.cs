﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.Configurations;

public class RoleConfiguration : IEntityTypeConfiguration<IdentityRole>
{
    public void Configure(EntityTypeBuilder<IdentityRole> builder)
    {
        builder.HasData(
            new IdentityRole
            {
                Id = "03ae51f3-6cdd-49ee-ac1a-d428b258719a",
                Name = "User",
                NormalizedName = "USER"
            },
            new IdentityRole
            {
                Id = "adb8c465-ad48-4879-a1d9-229eeea5bd95",
                Name = "Administrator",
                NormalizedName = "ADMINISTRATOR"
            }
        );
    }
}
