﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.Configurations;

public class UserRoleConfiguration : IEntityTypeConfiguration<IdentityUserRole<string>>
{
    public void Configure(EntityTypeBuilder<IdentityUserRole<string>> builder)
    {
        builder.HasData(
            new IdentityUserRole<string>
            {
                RoleId = "adb8c465-ad48-4879-a1d9-229eeea5bd95",
                UserId = "a6f060df-0d3b-446e-a1d2-bbd80a528ad7"
            },
            new IdentityUserRole<string>
            {
                RoleId = "03ae51f3-6cdd-49ee-ac1a-d428b258719a",
                UserId = "3055d30b-a83d-4917-984c-a1695237167e"
            }
        );
    }
}
