using EcommerceWeb.Contracts;
using EcommerceWeb.Models.Authentication;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace EcommerceWeb.Pages;

public partial class Login
{
    public LoginVM Model { get; set; }

    [Inject]
    public NavigationManager NavigationManager { get; set; }
    public string Message { get; set; }

    [Inject]
    private IAuthenticationService AuthenticationService { get; set; }

    [Inject]
    private AuthenticationStateProvider AuthenticationStateProvider { get; set; }

    public Login()
    {

    }

    protected override void OnInitialized()
    {
        Model = new LoginVM();
    }

    protected async Task HandleLogin()
    {
        if (await AuthenticationService.AuthenticateAsync(Model.UserName, Model.Password))
        {
            var loggedUser = await AuthenticationStateProvider.GetAuthenticationStateAsync();
            if (loggedUser != null)
            {
                if (loggedUser.User.IsInRole("User"))
                {
                    NavigationManager.NavigateTo("/products");
                }
                else
                {
                    NavigationManager.NavigateTo("/admin/products");
                }
            }
            else
            {
                NavigationManager.NavigateTo("/");
            }


            //var rolesClaim = loggedUser.User.FindFirst(ClaimTypes.Role);

            //if (rolesClaim != null)
            //{
            //    string[] roles = rolesClaim.Value.Split(',');
  
            //    if(roles.Any(a => a.ToLower() == "user"))
            //    {
            //        NavigationManager.NavigateTo("/products");
            //    } 
            //    else
            //    {
            //        NavigationManager.NavigateTo("/admin/products");
            //    }
            //} 
            //else
            //{
            //    NavigationManager.NavigateTo("/");
            //}
        }
        else
            Message = "Username/password isn't correct";
    }
}