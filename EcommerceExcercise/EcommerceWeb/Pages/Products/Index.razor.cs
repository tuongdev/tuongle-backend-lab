using EcommerceWeb.Contracts;
using EcommerceWeb.Models.Orders;
using EcommerceWeb.Models.Products;
using Microsoft.AspNetCore.Components;

namespace EcommerceWeb.Pages.Products;

public partial class Index
{
    [Inject]
    public NavigationManager _navigationManager { get; set; }

    [Inject]
    public IProductService _productService { get; set; }

    [Inject]
    public ICartService _cartService { get; set; }

    public List<ProductVM> Products { get; private set; }

    public string Message { get; set; } = string.Empty;

    public string SearchQuery { get; set; } = string.Empty;

    protected async Task AddToCartAsync(ProductVM product)
    {
        var newCartItem = new OrderDetailVM
        {
            Product = product,
            Amount = product.Price,
            Quantity = 1,
            CreateDate = DateTime.Now,
        };
        await _cartService.AddToCartAsync(newCartItem);
        _navigationManager.NavigateTo($"/orders/cart");
    }


    protected async void Search() 
    {
        if (!string.IsNullOrEmpty(SearchQuery))
        {
            Products = await _productService.Search(SearchQuery);
        }
        else
        {
            Products = await _productService.Get();
        }    
    }

    protected override async Task OnInitializedAsync()
    {
        Products = await _productService.Get();
    }
}