using Blazored.Toast.Services;
using EcommerceWeb.Contracts;
using EcommerceWeb.Models.Orders;
using Microsoft.AspNetCore.Components;

namespace EcommerceWeb.Pages.Orders;

public partial class Cart
{
    public string Message { get; private set; }
    
    [Inject]
    NavigationManager _navManager { get; set; }

    [Inject]
    ICartService _cartService { get; set; }

    [Inject]
    IOrderService _orderService { get; set; }

    [Inject]
    IToastService _toastService { get; set; }

    public static OrderVM CartModel = new OrderVM();

    protected override async Task OnInitializedAsync()
    {
        CartModel = await _cartService.GetCartAsync();
    }

    protected async Task UpdateQuantity(int quantity, int productId)
    {
        
        var orderDetail = CartModel.OrderDetails.FirstOrDefault(x => x.ProductId == productId);
        orderDetail.Quantity = quantity;
        orderDetail.Amount = orderDetail.Quantity * orderDetail.Quantity;
        CartModel.TotalPrice += orderDetail.Amount;

        StateHasChanged();
        await Task.CompletedTask;
    }

    protected async Task HandleOrder()
    {
        var response = await _orderService.SubmitOrder(CartModel);
        if (response.Success)
        {
            _toastService.ShowSuccess("Leave Type created Successfully");
            _navManager.NavigateTo("/products/");
        }
        Message = response.Message;
    }

}