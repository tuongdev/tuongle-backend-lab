using EcommerceWeb.Contracts;
using EcommerceWeb.Models.Products;
using Microsoft.AspNetCore.Components;

namespace EcommerceWeb.Pages.Admin.Products;

public partial class Index
{
    [Inject]
    public NavigationManager _navigationManager { get; set; }

    [Inject]
    public IProductService _productService { get; set; }

    public List<ProductVM> Products { get; private set; }

    public string Message { get; set; } = string.Empty;

    protected void EditProduct(int id)
    {
        _navigationManager.NavigateTo($"/admin/products/edit/{id}");
    }

    protected async Task DeleteProduct(int id)
    {
        var response = await _productService.DeleteProduct(id);
        if (response.Success)
        {
            await OnInitializedAsync();
        }
        else
        {
            Message = response.Message;
        }
    }

    protected override async Task OnInitializedAsync()
    {
        Products = await _productService.Get();
    }
}