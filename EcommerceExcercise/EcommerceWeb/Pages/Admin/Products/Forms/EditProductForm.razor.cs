using EcommerceWeb.Models.Products;
using Microsoft.AspNetCore.Components;

namespace EcommerceWeb.Pages.Admin.Products.Forms;

public partial class EditProductForm
{
    [Parameter] public bool Disabled { get; set; } = false;
    [Parameter] public ProductVM Product { get; set; }
    [Parameter] public string ButtonText { get; set; } = "Save";
    [Parameter] public EventCallback OnValidSubmit { get; set; }
}