using EcommerceWeb.Contracts;
using EcommerceWeb.Models.Products;
using Microsoft.AspNetCore.Components;

namespace EcommerceWeb.Pages.Admin.Products;

public partial class Edit
{
    [Inject]
    IProductService _productService { get; set; }

    [Inject]
    NavigationManager _navManager { get; set; }

    [Parameter]
    public int id { get; set; }

    public string Message { get; private set; }

    ProductVM Product = new ProductVM();

    protected async override Task OnParametersSetAsync()
    {
        Product = await _productService.Get(id);
    }

    protected async Task EditProduct()
    {
        var response = await _productService.UpdateProduct(id, Product);
        if (response.Success)
        {
            _navManager.NavigateTo("/admin/products");
        }
        Message = response.Message;
    }

}