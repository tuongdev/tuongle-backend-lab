﻿using AutoMapper;
using EcommerceWeb.Models.Categories;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.MappingProfiles;

public class CategoryProfile : Profile
{
    public CategoryProfile()
    {
        CreateMap<CategoryDto, CategoryVM>();
    }
}
