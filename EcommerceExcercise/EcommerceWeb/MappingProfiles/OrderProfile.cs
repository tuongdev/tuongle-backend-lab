﻿using AutoMapper;
using EcommerceWeb.Models.Orders;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.MappingProfiles;

public class OrderProfile : Profile
{
    public OrderProfile()
    {
        CreateMap<OrderDetailDto, OrderDetailVM>();
        CreateMap<CreateOrderCommand, OrderVM>().
            ForMember(dest => dest.OrderDetails, opt => opt.MapFrom(f => f.OrderDetails));
    }
}
