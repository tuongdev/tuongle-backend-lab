﻿using AutoMapper;
using EcommerceWeb.Models.Products;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.MappingProfiles;

public class ProductProfile : Profile
{
    public ProductProfile()
    {
        CreateMap<ProductDto, ProductVM>().ReverseMap();
        CreateMap<UpdateProductCommand, ProductVM>().ReverseMap();
    }
}
