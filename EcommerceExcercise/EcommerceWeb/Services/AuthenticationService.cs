﻿using Blazored.LocalStorage;
using EcommerceWeb.Contracts;
using EcommerceWeb.Providers;
using EcommerceWeb.Services.Base;
using Microsoft.AspNetCore.Components.Authorization;

namespace EcommerceWeb.Services;

public class AuthenticationService : BaseHttpServiceBase, IAuthenticationService
{
    private readonly AuthenticationStateProvider _authenticationStateProvider;

    public AuthenticationService(IClient client,
                                 ILocalStorageService localStorageService,
                                 AuthenticationStateProvider authenticationStateProvider) : base(client, localStorageService)
    {
        this._authenticationStateProvider = authenticationStateProvider;
    }

    public async Task<bool> AuthenticateAsync(string userName, string password)
    {
        try
        {
            AuthRequest request = new() { UserName = userName, Password = password };
            var authenticationResponse = await _client.LoginAsync(request);

            if (!string.IsNullOrEmpty(authenticationResponse.Token))
            {
                await _localStorageService.SetItemAsync("token", authenticationResponse.Token);
                await ((ApiAuthenticationStateProvider)_authenticationStateProvider).LoggedIn();

                return true;
            }
            return false;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public async Task LogOut()
    {
        await _localStorageService.RemoveItemAsync("token");
    }

    public async Task<bool> RegisterAsync(string firstName, string lastName, string userName, string password)
    {
        RegistrationRequest registrationRequest = new() { FirstName = firstName, LastName = lastName, UserName = userName, Password = password };
        var response = await _client.RegisterAsync(registrationRequest);

        if (!string.IsNullOrEmpty(response.UserId))
        {
            return true;
        }

        return false;
    }
}
