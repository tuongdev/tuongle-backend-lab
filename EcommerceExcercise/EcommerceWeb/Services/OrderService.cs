﻿using AutoMapper;
using Blazored.LocalStorage;
using EcommerceWeb.Contracts;
using EcommerceWeb.Enums;
using EcommerceWeb.Models.Orders;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.Services;

public class OrderService : BaseHttpServiceBase, IOrderService
{
    private readonly IMapper _mapper;

    public OrderService(IClient client, IMapper mapper, ILocalStorageService localStorageService) : base(client, localStorageService)
    {
        this._mapper = mapper;
    }

    public async Task<Response<Guid>> SubmitOrder(OrderVM order)
    {
        try
        {
            await AddBearerToken();
            var createOrderCommand = new CreateOrderCommand
            {
                CreatedDate = DateTime.Now,
                TotalPrice = order.TotalPrice,
                Status = (int)OrderStatus.Open,
                OrderDetails = order.OrderDetails?.Select(s => new OrderDetailDto
                {
                    Amount = s.Amount,
                    CreateDate = DateTime.Now,
                    OrderId = s.OrderId,
                    Quantity = s.Quantity,
                    ProductId = s.Product.Id,
                }).ToList()
            };
            await _client.OrdersAsync(createOrderCommand);

            return new Response<Guid>()
            {
                Success = true,
            };
        }
        catch (ApiException ex)
        {

            return ConvertApiExceptions<Guid>(ex);
        }
    }
}
