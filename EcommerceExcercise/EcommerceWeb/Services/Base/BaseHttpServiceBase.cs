﻿using Blazored.LocalStorage;

namespace EcommerceWeb.Services.Base;

public class BaseHttpServiceBase
{
    protected IClient _client;
    protected readonly ILocalStorageService _localStorageService;

    public BaseHttpServiceBase(IClient client, ILocalStorageService localStorageService)
    {
        _client = client;
        _localStorageService = localStorageService;
    }

    protected Response<Guid> ConvertApiExceptions<Guid>(ApiException ex)
    {
        if (ex.StatusCode == 400)
        {
            return new Response<Guid>
            {
                Message = "Invalid data",
                ValidationErrors = ex.Response,
                Success = false
            };
        }
        else if(ex.StatusCode == 404)
        {
            return new Response<Guid>
            {
                Message = "Data was not found",
                ValidationErrors = ex.Response,
                Success = false
            };
        } else
        {
            return new Response<Guid>
            {
                Message = "Something went wrong, try again",
                ValidationErrors = ex.Response,
                Success = false
            };
        }
    }

    protected async Task AddBearerToken()
    {
        if (await _localStorageService.ContainKeyAsync("token"))
        {
            _client.HttpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer",
                await _localStorageService.GetItemAsync<string>("token"));
        }
    }
}