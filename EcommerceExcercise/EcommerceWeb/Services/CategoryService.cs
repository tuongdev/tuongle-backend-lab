﻿using AutoMapper;
using Blazored.LocalStorage;
using EcommerceWeb.Contracts;
using EcommerceWeb.Models.Categories;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.Services;

public class CategoryService : BaseHttpServiceBase, ICategoryService
{
    private readonly IMapper _mapper;

    public CategoryService(IClient client, IMapper mapper, ILocalStorageService localStorageService) : base(client, localStorageService)
    {
        this._client = client;
        this._mapper = mapper;
    }

    public Task<Response<Guid>> CreateCategory(CategoryVM categoryVM)
    {
        throw new NotImplementedException();
    }

    public async Task<List<CategoryVM>> GetCategories()
    {
        var categories = await _client.CategoriesAllAsync();
        var result = _mapper.Map<List<CategoryVM>>(categories);
        return result;

    }

    public async Task<CategoryVM> GetCategoryById(int id)
    {
        var category = await _client.CategoriesAsync(id);
        var result = _mapper.Map<CategoryVM>(category);
        return result;
    }
}
