﻿using AutoMapper;
using Blazored.LocalStorage;
using EcommerceWeb.Contracts;
using EcommerceWeb.Models.Products;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.Services;

public class ProductService : BaseHttpServiceBase, IProductService
{
    private readonly IMapper _mapper;

    public ProductService(IClient client, IMapper mapper, ILocalStorageService localStorageService) : base(client, localStorageService)
    {
        this._mapper = mapper;
    }

    public async Task<Response<Guid>> DeleteProduct(int id)
    {
        try
        {
            await AddBearerToken();
            //await _client.De(id);
            return new Response<Guid>() { Success = true };
        }
        catch (ApiException ex)
        {
            return ConvertApiExceptions<Guid>(ex);
        }
    }

    public async Task<List<ProductVM>> Get()
    {
        await AddBearerToken();
        var products = await _client.ProductsAllAsync();
        var result = _mapper.Map<List<ProductVM>>(products);
        return result;
    }

    public async Task<ProductVM> Get(int id)
    {
        await AddBearerToken();

        var product = await _client.ProductsGETAsync(id);

        var result = _mapper.Map<ProductVM>(product);

        return result;
    }

    public async Task<List<ProductVM>> Search(string query)
    {
        await AddBearerToken();
        var products = await _client.SearchAsync(query);

        var result = _mapper.Map<List<ProductVM>>(products);

        return result;
    }

    public async Task<Response<Guid>> UpdateProduct(int id, ProductVM productVM)
    {
        try
        {
            await AddBearerToken();
            var updateProductCommand = _mapper.Map<UpdateProductCommand>(productVM);
            await _client.ProductsPUTAsync(id.ToString(), updateProductCommand);
            return new Response<Guid>()
            {
                Success = true,
            };
        }
        catch (ApiException ex)
        {
            return ConvertApiExceptions<Guid>(ex);
        }
    }
}
