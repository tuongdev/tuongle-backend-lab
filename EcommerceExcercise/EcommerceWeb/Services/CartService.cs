﻿using EcommerceWeb.Contracts;
using EcommerceWeb.Enums;
using EcommerceWeb.Models.Orders;
using Microsoft.JSInterop;
using System.Text.Json;

namespace EcommerceWeb.Services;

public class CartService : ICartService
{
    private OrderVM cart = new OrderVM();
    private const string CartStorageKey = "cart_items";

    private readonly IJSRuntime _jsRuntime;

    public CartService(IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
    }
    public async Task AddToCartAsync(OrderDetailVM item)
    {
        //await LoadCartFromSessionStorage();

        if(cart.OrderDetails == null)
            cart.OrderDetails = new List<OrderDetailVM>();
        
        cart.OrderDetails.Add(item);
        cart.TotalPrice = cart.OrderDetails.Sum(s => s.Amount);
        cart.Status = (int)OrderStatus.Open;
        await SaveCartToSessionStorage(cart);
    }

    public async Task<OrderVM> GetCartAsync()
    {
        await LoadCartFromSessionStorage();
        return cart;
    }
    private async Task LoadCartFromSessionStorage()
    {
        var cartJson = await _jsRuntime.InvokeAsync<string>("sessionStorage.getItem", CartStorageKey);
        if (!string.IsNullOrEmpty(cartJson))
        {
            cart = JsonSerializer.Deserialize<OrderVM>(cartJson);
        }
        else
        {
            cart = new OrderVM();
        }
    }

    private async Task SaveCartToSessionStorage(OrderVM cart)
    {
        var cartJson = JsonSerializer.Serialize(cart);
        await _jsRuntime.InvokeVoidAsync("sessionStorage.setItem", CartStorageKey, cartJson);
    }

    private async Task RemoveCartFromSessionStorage()
    {
        await _jsRuntime.InvokeVoidAsync("sessionStorage.removeItem", CartStorageKey);
    }

    public async Task ClearCartAsync()
    {
        cart = new OrderVM();
        await RemoveCartFromSessionStorage();
    }

    public async Task UpdateQuantityAsync(int productId, int newQuantity)
    {
        var productUpdate = cart.OrderDetails.FirstOrDefault(item => item.Id == productId);
        if (productUpdate != null)
        {
            productUpdate.Quantity = newQuantity;
            productUpdate.Amount = newQuantity * productUpdate.Product.Price;
            cart.TotalPrice = cart.OrderDetails.Sum(s => s.Amount);
            await ClearCartAsync();
            await SaveCartToSessionStorage(cart);
        }
    }

    public async Task RemoveFromCartAsync(int productId)
    {
        await LoadCartFromSessionStorage();
        var productRemove = cart.OrderDetails.FirstOrDefault(item => item.Id == productId);

        if (productRemove != null)
        {
            cart.OrderDetails.Remove(productRemove);
            await ClearCartAsync();
            await SaveCartToSessionStorage(cart);
        }
    }
}
