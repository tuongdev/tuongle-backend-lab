﻿namespace EcommerceWeb.Enums;

public enum OrderStatus
{
    Open,
    Completed
}
