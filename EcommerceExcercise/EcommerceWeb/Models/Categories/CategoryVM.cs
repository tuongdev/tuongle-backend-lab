﻿using System.ComponentModel.DataAnnotations;

namespace EcommerceWeb.Models.Categories;

public class CategoryVM
{
    public int Id { get; set; }
    [Required]
    public string Name { get; set; } = string.Empty;
    public DateTime CreateDate { get; set; }
}
