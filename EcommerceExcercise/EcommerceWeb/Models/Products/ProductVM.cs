﻿using EcommerceWeb.Models.Categories;

namespace EcommerceWeb.Models.Products;

public class ProductVM
{
    public int Id { get; set; }
    public int CategoryId { get; set; }
    public DateTime CreateDate { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Rating { get; set; }
    public double Price { get; set; }
    public CategoryVM? Category { get; set; }
    public int Remaining { get; set; }
}
