﻿using EcommerceWeb.Models.Products;

namespace EcommerceWeb.Models.Orders;

public class OrderDetailVM
{
    public int Id { get; set; }
    public DateTime CreateDate { get; set; }
    public int OrderId { get; set; }
    public ProductVM? Product { get; set; }
    public int ProductId { get; set; }
    public int Quantity { get; set; }
    public double Amount { get; set; }
}
