﻿namespace EcommerceWeb.Models.Orders;

public class OrderVM
{
    public int Id { get; set; }
    public double TotalPrice { get; set; }
    public int Status { get; set; }
    public List<OrderDetailVM>? OrderDetails { get; set; }
    public DateTime CreatedDate { get; set; }
}
