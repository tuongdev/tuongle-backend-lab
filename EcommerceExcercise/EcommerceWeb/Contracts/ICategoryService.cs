﻿using EcommerceWeb.Models.Categories;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.Contracts;

public interface ICategoryService
{
    Task<List<CategoryVM>> GetCategories();
    Task<CategoryVM> GetCategoryById(int id);
    Task<Response<Guid>> CreateCategory(CategoryVM categoryVM);
}
