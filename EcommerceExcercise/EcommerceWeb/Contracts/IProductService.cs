﻿using EcommerceWeb.Models.Products;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.Contracts;

public interface IProductService
{
    Task<List<ProductVM>> Search(string query);
    Task<List<ProductVM>> Get();
    Task<ProductVM> Get(int id);
    Task<Response<Guid>> UpdateProduct(int id, ProductVM productVM);
    Task<Response<Guid>> DeleteProduct(int id);

}
