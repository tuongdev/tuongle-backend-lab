﻿using EcommerceWeb.Models.Orders;

namespace EcommerceWeb.Contracts;

public interface ICartService
{
    Task<OrderVM> GetCartAsync();
    Task AddToCartAsync(OrderDetailVM item);
    Task ClearCartAsync();
    Task UpdateQuantityAsync(int productId, int newQuantity);
    Task RemoveFromCartAsync(int productId);
}
