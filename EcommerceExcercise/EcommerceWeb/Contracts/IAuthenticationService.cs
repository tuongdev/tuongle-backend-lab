﻿namespace EcommerceWeb.Contracts;

public interface IAuthenticationService
{
    Task<bool> AuthenticateAsync(string userName, string password);
    Task<bool> RegisterAsync(string firstName, string lastName, string userName, string password);
    Task LogOut();
}
