﻿using EcommerceWeb.Models.Orders;
using EcommerceWeb.Services.Base;

namespace EcommerceWeb.Contracts;

public interface IOrderService
{
    Task<Response<Guid>> SubmitOrder(OrderVM order);
}
