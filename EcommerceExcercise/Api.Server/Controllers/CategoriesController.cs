﻿using Application.DTOs.Categories;
using Application.Features.Category.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Api.Server.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CategoriesController : ControllerBase
{
    private readonly IMediator _mediator;

    public CategoriesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<List<CategoryDto>> Get()
    {
        var categories = await _mediator.Send(new GetCategoriesQuery());
        return categories;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<CategoryDto>> Get(int id)
    {
        var category = await _mediator.Send(new GetCategoryDetailQuery(id));
        return Ok(category);
    }
}
