﻿using Application.DTOs.Products;
using Application.Features.Product.Commands;
using Application.Features.Product.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Api.Server.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize]
public class ProductsController : ControllerBase
{
    private readonly IMediator _mediator;

    public ProductsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<List<ProductDto>> Get()
    {
        var products = await _mediator.Send(new GetAllProductsQuery());
        return products;
    }

    [HttpGet("{id}")]
    public async Task<ProductDto> Get(int id)
    {
        var product = await _mediator.Send(new GetProductDetailQuery(id));
        return product;
    }

    [HttpPost]
    [ProducesResponseType(201)]
    [ProducesResponseType(400)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> Add(CreateProductCommand product)
    {
        var response = await _mediator.Send(product);

        return CreatedAtAction(nameof(Add), new { id = response });
    }

    [HttpGet("search/{searchQuery}")]
    [SwaggerOperation]
    [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(List<ProductDto>))]
    public async Task<List<ProductDto>> Search(string? searchQuery = "")
    {
        var products = await _mediator.Send(new SearchProductsQuery { SearchQuery = searchQuery});
        return products;
    }

    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(400)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesDefaultResponseType]
    public async Task<ActionResult> Put(UpdateProductCommand product)
    {
        await _mediator.Send(product);
        return NoContent();
    }
}
