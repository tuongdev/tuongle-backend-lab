﻿using Microsoft.AspNetCore.Mvc;

namespace Api.Server.Models
{
    public class ValidationProblemDetail : ProblemDetails
    {
        public IDictionary<string, string[]> Errors { get; set; } = new Dictionary<string, string[]>();
    }
}
