﻿using Application.Contracts.Persistence;
using Domain;
using Microsoft.EntityFrameworkCore;
using Persistence.DatabaseContext;
using System.Linq.Expressions;

namespace Persistence.Repositories;

public class ProductRepository : GenericRepository<Product>, IProductRepository
{
    public ProductRepository(EcommerceDbContext context) : base(context)
    {
    }

    public async Task<bool> IsProductNameUnique(string name)
    {
        return await _context.Products.AnyAsync(p => p.Name == name) == false;
    }

    public async Task<IReadOnlyList<Product>> GetAllProducts()
    {
        return await _context.Products.Include(c => c.Category).ToListAsync();
    }

    public async Task<Product> GetProductDetail(int id)
    {
        return await _context.Products.FirstOrDefaultAsync(f => f.Id == id);
    }

    public async Task<IReadOnlyList<Product>> Search(string? searchQuery)
    {
        if (string.IsNullOrEmpty(searchQuery))
            return _context.Products.Include(c => c.Category).ToList();

        Expression<Func<Product, bool>> searchFilter = p =>
            EF.Functions.FreeText(p.Name, searchQuery) ||
            EF.Functions.FreeText(p.Description, searchQuery) ||
            EF.Functions.FreeText(p.Category.Name, searchQuery);

        return await _context.Products.Include(c => c.Category).Where(searchFilter).ToListAsync();
    }

}
