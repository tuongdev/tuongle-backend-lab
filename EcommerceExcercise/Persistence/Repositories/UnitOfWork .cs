﻿using Application.Contracts.Persistence;
using Persistence.DatabaseContext;

namespace Persistence.Repositories;

public class UnitOfWork : IUnitOfWork, IDisposable
{
    private readonly EcommerceDbContext _context;

    public ICategoryRepository Categories { get; private set; }
    public IProductRepository Products { get; private set; }
    public IOrderRepository Orders { get; private set; }

    public UnitOfWork(EcommerceDbContext context)
    {
        _context = context;

        Categories = new CategoryRepository(context);
        Products = new ProductRepository(context);
        Orders = new OrderRepository(context);
    }

    public async Task CommitAsync(CancellationToken cancellationToken)
    {
        await _context.SaveChangesAsync(cancellationToken);
    }

    public void Dispose()
    {
        _context.Dispose();
    }
}