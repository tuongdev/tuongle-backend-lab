﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Persistence.Migrations
{
    /// <inheritdoc />
    public partial class MigrationToAddFullText : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                sql: "IF NOT EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name = 'ProductCatalog') BEGIN  CREATE FULLTEXT CATALOG ProductCatalog; END CREATE FULLTEXT INDEX ON dbo.Products (Name LANGUAGE 1033,Description LANGUAGE 1033) KEY INDEX PK_Products ON ProductCatalog",
                suppressTransaction: true);

            migrationBuilder.Sql(
                sql: "IF NOT EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name = 'CategoryCatalog') BEGIN CREATE FULLTEXT CATALOG CategoryCatalog; END CREATE FULLTEXT INDEX ON dbo.Categories (Name LANGUAGE 1033) KEY INDEX PK_Categories ON CategoryCatalog",
                suppressTransaction: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
