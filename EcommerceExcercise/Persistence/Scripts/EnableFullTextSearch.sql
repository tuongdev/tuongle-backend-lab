﻿USE db_ecommerce_lab;


ALTER DATABASE db_ecommerce_lab
SET FILESTREAM (NON_TRANSACTED_ACCESS = FULL, DIRECTORY_NAME = N'FTS');


IF NOT EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name = 'ProductCatalog')
BEGIN
    CREATE FULLTEXT CATALOG ProductCatalog;
END

CREATE FULLTEXT INDEX ON dbo.Products
(
    Name LANGUAGE 1033,
    Description LANGUAGE 1033
)
KEY INDEX PK_Products ON ProductCatalog


IF NOT EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name = 'CategoryCatalog')
BEGIN
    CREATE FULLTEXT CATALOG CategoryCatalog;
END


CREATE FULLTEXT INDEX ON dbo.Categories
(
    Name LANGUAGE 1033
)
KEY INDEX PK_Categories ON CategoryCatalog

WITH STOPLIST = SYSTEM;

