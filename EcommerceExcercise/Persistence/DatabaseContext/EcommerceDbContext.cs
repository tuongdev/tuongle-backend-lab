﻿using Domain;
using Domain.Common;
using Microsoft.EntityFrameworkCore;
using Persistence.Configurations;

namespace Persistence.DatabaseContext;

public class EcommerceDbContext : DbContext
{
    public EcommerceDbContext(DbContextOptions<EcommerceDbContext> options) : base(options)
    {
        
    }

    public DbSet<Product> Products { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderDetail> OrderDetails { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(EcommerceDbContext).Assembly);

        base.OnModelCreating(modelBuilder);
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        foreach(var entity in base.ChangeTracker.Entries<BaseEntity>().Where(w => w.State == EntityState.Added || w.State == EntityState.Modified))
        {
            entity.Entity.CreateDate = DateTime.Now;
        }

        return base.SaveChangesAsync(cancellationToken);
    }
}
