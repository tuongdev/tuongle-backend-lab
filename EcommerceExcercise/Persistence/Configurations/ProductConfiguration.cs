﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations;

public class ProductConfiguration : IEntityTypeConfiguration<Product>
{
    public void Configure(EntityTypeBuilder<Product> builder)
    {
        builder.HasData(
            new Product
            {
                Id = 1,
                Name = "Product A",
                CategoryId = 1,
                Description = "Product A description",
                CreateDate = DateTime.Now,
                Price = 10000,
                Rating = 0,
                Remaining = 50
            },
            new Product
            {
                Id = 2,
                Name = "Product B",
                CategoryId = 2,
                Description = "Product B description",
                CreateDate = DateTime.Now,
                Price = 13000,
                Rating = 0,
                Remaining = 100,
            }
        );

        builder.Property(q => q.Name).IsRequired();

    }
}
