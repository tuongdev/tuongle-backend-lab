﻿using Application.Models.Identity;

namespace Application.Contracts.Identity;

public interface IUserService
{
    Task<User> GetUser(string userId);
    Task<IList<string>> GetUserRoles(string userId);
}
