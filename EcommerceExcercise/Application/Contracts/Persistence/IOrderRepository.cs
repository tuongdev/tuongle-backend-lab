﻿using Domain;

namespace Application.Contracts.Persistence;

public interface IOrderRepository : IGenericRepository<Order>
{

}
