﻿using Domain;

namespace Application.Contracts.Persistence;

public interface IProductRepository : IGenericRepository<Product>
{
    Task<bool> IsProductNameUnique(string name);

    Task<IReadOnlyList<Product>> GetAllProducts();

    Task<Product> GetProductDetail(int id);

    Task<IReadOnlyList<Product>> Search(string? searchQuery);
}
