﻿namespace Application.Contracts.Persistence;

public interface IUnitOfWork
{
    ICategoryRepository Categories { get; }
    IProductRepository Products { get; }
    IOrderRepository Orders { get; }
    Task CommitAsync(CancellationToken cancellationToken);
}
