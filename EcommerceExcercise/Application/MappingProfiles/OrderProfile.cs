﻿using Application.DTOs.Orders;
using Application.Features.Order.Commands;
using AutoMapper;
using Domain;

namespace Application.MappingProfiles;

public class OrderProfile : Profile
{
    public OrderProfile()
    {
        CreateMap<OrderDto, Order>().ReverseMap();
        CreateMap<OrderDetailDto, OrderDetail>().ReverseMap()
            .ForMember(dest => dest.Product, opt => opt.MapFrom(f => f.Product));

        CreateMap<CreateOrderCommand, Order>()
            .ForMember(dest => dest.OrderDetails, opt => opt.MapFrom(f => f.OrderDetails));
    }
}
