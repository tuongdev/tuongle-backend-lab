﻿using Application.DTOs.Categories;
using Application.DTOs.Products;
using Application.Features.Product.Commands;
using AutoMapper;
using Domain;

namespace Application.MappingProfiles;

public class ProductProfile : Profile
{
    public ProductProfile()
    {
        CreateMap<ProductDto, CategoryDto>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Category.Id))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Category.Name));

        CreateMap<CategoryDto, ProductDto>();

        CreateMap<Product, ProductDto>().ReverseMap()
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));


        CreateMap<CreateProductCommand, Product>();
        CreateMap<UpdateProductCommand, Product>();
    }
}
