﻿using System.ComponentModel.DataAnnotations;

namespace Application.Models.Identity;

public class RegistrationRequest
{
    [Required]
    public string UserName { get; set; }

    [Required]
    public string FirstName { get; set; }

    [Required]
    public string LastName { get; set; }

    [Required]
    [MinLength(6)]
    public string Password { get; set; }

    public string Role { get; set; }
}
