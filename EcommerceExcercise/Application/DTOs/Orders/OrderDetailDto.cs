﻿using Application.DTOs.Products;

namespace Application.DTOs.Orders;

public class OrderDetailDto
{
    public int Id { get; set; }
    public DateTime CreateDate { get; set; }
    public int OrderId { get; set; }
    public ProductDto? Product { get; set; }
    public int ProductId { get; set; }
    public int Quantity { get; set; }
    public double Amount { get; set; }
}
