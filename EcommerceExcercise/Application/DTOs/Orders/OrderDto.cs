﻿namespace Application.DTOs.Orders;

public class OrderDto
{
    public int Id { get; set; }
    public double TotalPrice { get; set; }
    public int Status { get; set; }
    public List<OrderDetailDto>? OrderDetails { get; set; }
    public DateTime CreatedDate { get; set; }
}
