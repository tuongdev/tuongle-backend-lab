﻿using Application.Contracts.Logging;
using Application.Contracts.Persistence;
using Application.Exceptions;
using AutoMapper;
using FluentValidation;
using MediatR;

namespace Application.Features.Product.Commands;

public class CreateProductCommand : IRequest<int>
{
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public double Price { get; set; }
    public int Rating { get; set; }
    public int CategoryId { get; set; }
    public int Remaining { get; set; }
}

public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
{
    private readonly IUnitOfWork _unitOfWork;

    public CreateProductCommandValidator(IUnitOfWork unitOfWork)
    {
        RuleFor(request => request)
            .NotEmpty().WithMessage("Not empty")
            .NotNull().WithMessage("Not null");

        RuleFor(request => request.CategoryId)
            .NotNull().WithMessage("{PropertyName} is required");

        RuleFor(request => request.Price)
            .NotNull().WithMessage("{PropertyName} is required")
            .GreaterThan(0).WithMessage("{PropertyName} must greater than 0");

        RuleFor(request => request)
            .MustAsync(ProductNameUnique)
            .WithMessage("Product Name already exists");

        RuleFor(request => request.Remaining)
            .NotEmpty()
            .NotNull().WithMessage("{PropertyName} is required")
            .WithMessage("Reamining must be greater than zero");

        _unitOfWork = unitOfWork;
    }

    private async Task<bool> ProductNameUnique(CreateProductCommand command, CancellationToken cancellationToken)
    {
        return await _unitOfWork.Products.IsProductNameUnique(command.Name);
    }
}

public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, int>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAppLogger<CreateProductCommandHandler> _logger;

    public CreateProductCommandHandler(IMapper mapper,
                                       IUnitOfWork unitOfWork,
                                       IAppLogger<CreateProductCommandHandler> logger)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
        _logger = logger;
    }

    public async Task<int> Handle(CreateProductCommand request, CancellationToken cancellationToken)
    {
        var validator = new CreateProductCommandValidator(_unitOfWork);

        var validatorResult = await validator.ValidateAsync(request, cancellationToken);

        if (!validatorResult.IsValid)
        {
            _logger.LogWarning("Invalid request {0}", nameof(request));

            throw new BadRequestException("Invalid product", validatorResult);
        }

        var product = _mapper.Map<Domain.Product>(request);

        await _unitOfWork.Products.CreateAsync(product);

        return product.Id;
    }
}
