﻿using Application.Contracts.Logging;
using Application.Contracts.Persistence;
using Application.Exceptions;
using AutoMapper;
using FluentValidation;
using MediatR;

namespace Application.Features.Product.Commands;

public class UpdateProductCommand : IRequest<Unit>
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int CategoryId { get; set; }
    public int Rating { get; set; }
    public double Price { get; set; }
    public int Remaining { get; set; }
    public DateTime CreateDate { get; set; }
}

public class UpdateProductCommandValidator : AbstractValidator<UpdateProductCommand>
{
    private readonly IUnitOfWork _unitOfWork;

    public UpdateProductCommandValidator(IUnitOfWork unitOfWork)
    {
        RuleFor(p => p.Id)
            .NotNull()
            .MustAsync(LeaveTypeMustExist);

        RuleFor(p => p.Name)
            .NotEmpty().WithMessage("{PropertyName} is required")
            .NotNull()
            .MaximumLength(70).WithMessage("{PropertyName} must be fewer than 70 characters");

        RuleFor(p => p.Remaining)
            .GreaterThan(1).WithMessage("{PropertyName} cannot be less than 1");

        RuleFor(p => p.Price)
            .GreaterThan(1).WithMessage("{PropertyName} cannot be less than 1");
        this._unitOfWork = unitOfWork;
    }

    private async Task<bool> LeaveTypeMustExist(int id, CancellationToken arg2)
    {
        var product = await _unitOfWork.Products.GetByIdAsync(id);
        return product != null;
    }
}

public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, Unit>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAppLogger<UpdateProductCommandHandler> _logger;

    public UpdateProductCommandHandler(IMapper mapper, IUnitOfWork unitOfWork, IAppLogger<UpdateProductCommandHandler> logger)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
        this._logger = logger;
    }

    public async Task<Unit> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
    {
        var validator = new UpdateProductCommandValidator(_unitOfWork);
        var validationResult = await validator.ValidateAsync(request);

        if (validationResult.Errors.Any())
        {
            _logger.LogWarning("Validation errors in update request for {0} - {1}", nameof(Product), request.Id);
            throw new BadRequestException("Invalid Product", validationResult);
        }

        var productToUpdate = _mapper.Map<Domain.Product>(request);

        await _unitOfWork.Products.UpdateAsync(productToUpdate);

        return Unit.Value;
    }
}