﻿using Application.Contracts.Persistence;
using Application.DTOs.Products;
using AutoMapper;
using MediatR;

namespace Application.Features.Product.Queries;

public record class GetProductDetailQuery(int Id) : IRequest<ProductDto>;

public class GetProductDetailQueryHandler : IRequestHandler<GetProductDetailQuery, ProductDto>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;

    public GetProductDetailQueryHandler(IMapper mapper, IUnitOfWork unitOfWork)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }

    public async Task<ProductDto> Handle(GetProductDetailQuery request, CancellationToken cancellationToken)
    {
        var productDetail = await _unitOfWork.Products.GetByIdAsync(request.Id);

        var result = _mapper.Map<ProductDto>(productDetail);

        return result;
    } 
}