﻿using Application.Contracts.Persistence;
using Application.DTOs.Products;
using AutoMapper;
using MediatR;


namespace Application.Features.Product.Queries;

public record class SearchProductsQuery : IRequest<List<ProductDto>>
{
    public string? SearchQuery { get; set; }
}
public class SearchProductsQueryHandler : IRequestHandler<SearchProductsQuery, List<ProductDto>>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;

    public SearchProductsQueryHandler(IMapper mapper, IUnitOfWork unitOfWork)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }

    public async Task<List<ProductDto>> Handle(SearchProductsQuery request, CancellationToken cancellationToken)
    {
        var products = await _unitOfWork.Products.Search(request.SearchQuery);

        return _mapper.Map<List<ProductDto>>(products);
    }
}
