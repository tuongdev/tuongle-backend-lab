﻿using Application.Contracts.Logging;
using Application.Contracts.Persistence;
using Application.DTOs.Products;
using AutoMapper;
using MediatR;

namespace Application.Features.Product.Queries;

public record GetAllProductsQuery : IRequest<List<ProductDto>>;

public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, List<ProductDto>>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAppLogger<GetAllProductsQueryHandler> _logger;

    public GetAllProductsQueryHandler(IMapper mapper,
                                      IUnitOfWork unitOfWork,
                                      IAppLogger<GetAllProductsQueryHandler> logger)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
        _logger = logger;
    }

    public async Task<List<ProductDto>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
    {
        var products = await _unitOfWork.Products.GetAllProducts();

        var result = _mapper.Map<List<ProductDto>>(products);

        return result;
    }
}