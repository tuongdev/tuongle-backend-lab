﻿using Application.Contracts.Logging;
using Application.Contracts.Persistence;
using Application.DTOs.Categories;
using AutoMapper;
using MediatR;

namespace Application.Features.Category.Queries;

public record class GetCategoryDetailQuery(int id) : IRequest<CategoryDto>;

public class GetCategoryDetailQueryHandler : IRequestHandler<GetCategoryDetailQuery, CategoryDto>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAppLogger<GetCategoryDetailQueryHandler> _logger;

    public GetCategoryDetailQueryHandler(IMapper mapper, IUnitOfWork unitOfWork, IAppLogger<GetCategoryDetailQueryHandler> logger)
    {
        this._mapper = mapper;
        this._unitOfWork = unitOfWork;
        this._logger = logger;
    }

    public async Task<CategoryDto> Handle(GetCategoryDetailQuery request, CancellationToken cancellationToken)
    {
        var category = await _unitOfWork.Categories.GetByIdAsync(request.id);
        var result = _mapper.Map<CategoryDto>(category);
        return result;
    }
}
