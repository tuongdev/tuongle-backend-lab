﻿using Application.Contracts.Logging;
using Application.Contracts.Persistence;
using Application.DTOs.Categories;
using AutoMapper;
using MediatR;

namespace Application.Features.Category.Queries;

public record GetCategoriesQuery : IRequest<List<CategoryDto>>;

public class GetCategoriesQueryHandler : IRequestHandler<GetCategoriesQuery, List<CategoryDto>>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAppLogger<GetCategoriesQueryHandler> _logger;

    public GetCategoriesQueryHandler(IMapper mapper,
                                     IUnitOfWork unitOfWork,
                                     IAppLogger<GetCategoriesQueryHandler> logger)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
        _logger = logger;
    }

    public async Task<List<CategoryDto>> Handle(GetCategoriesQuery request, CancellationToken cancellationToken)
    {
        var categories = await _unitOfWork.Categories.GetAsync();

        var result = _mapper.Map<List<CategoryDto>>(categories);

        return result;
    }
}
