﻿using Domain;

namespace Application.Features.Order.Queries;

public class OrderDto
{
    public int Id { get; set; }
    public DateTime Create { get; set; }
    public double TotalPrice { get; set; }
    public int Status { get; set; }
    public IEnumerable<OrderDetail> OrderDetails { get; set; }
}
