﻿using Application.Contracts.Logging;
using Application.Contracts.Persistence;
using Application.DTOs.Orders;
using Application.Exceptions;
using AutoMapper;
using FluentValidation;
using MediatR;

namespace Application.Features.Order.Commands;

public class CreateOrderCommand : IRequest<int>
{
    public double TotalPrice { get; set; }
    public int Status { get; set; }
    public List<OrderDetailDto>? OrderDetails { get; set; }
    public DateTime CreatedDate { get; set; }
}

public class CreateOrderCommandValidator : AbstractValidator<CreateOrderCommand>
{
    public CreateOrderCommandValidator()
    {
        RuleFor(request => request)
            .NotEmpty().WithMessage("Not empty")
            .NotNull().WithMessage("Not null");

        RuleFor(request => request)
            .MustAsync(MustHaveProducts)
            .WithMessage("Must have at least one product");

    }
    private async Task<bool> MustHaveProducts(CreateOrderCommand command, CancellationToken cancellationToken)
    {
        return (command.OrderDetails != null && command.OrderDetails.Any());
    }
}

public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, int>
{
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAppLogger<CreateOrderCommandHandler> _logger;

    public CreateOrderCommandHandler(IMapper mapper, IUnitOfWork unitOfWork, IAppLogger<CreateOrderCommandHandler> logger)
    {
        this._mapper = mapper;
        this._unitOfWork = unitOfWork;
        this._logger = logger;
    }

    public async Task<int> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
    {
        var validator = new CreateOrderCommandValidator();
        var validatorResult = await validator.ValidateAsync(request, cancellationToken);

        if (!validatorResult.IsValid)
        {
            _logger.LogWarning("Invalid request {0}", nameof(request));

            throw new BadRequestException("Invalid product", validatorResult);
        }

        var order = _mapper.Map<Domain.Order>(request);

        await _unitOfWork.Orders.CreateAsync(order);

        return order.Id;
    }
}