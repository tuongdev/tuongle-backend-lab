using Domain;
using Microsoft.EntityFrameworkCore;
using Persistence.DatabaseContext;
using Shouldly;

namespace Persistence.IntegrationTests;

public class EcommerceDbContextTests
{
    private EcommerceDbContext _dbContext;

    public EcommerceDbContextTests()
    {
        var dbOptions = new DbContextOptionsBuilder<EcommerceDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

        _dbContext = new EcommerceDbContext(dbOptions);
    }

    [Fact]
    public async void Save_SetCreateDate()
    {
        var category = new Category
        {
            Id = 1,
            Name = "Test Type A"
        };

        await _dbContext.Categories.AddAsync(category);
        await _dbContext.SaveChangesAsync();

        category.CreateDate.ShouldNotBeNull();
    }
}