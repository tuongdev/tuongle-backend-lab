- Introduction
Welcome to tuongle-lab project

- Getting Started
To get started with the project, follow the instructions below to set up your development environment and run the project locally.

- Prerequisites
.NET 7
.MS SQL (enable fulltext search feature)
.Nswag Studio
.Visual Studio 2022

- Project Overview
The project follows the principles of Clean Architecture, providing a clear separation of concerns and promoting maintainability and scalability. 
It also adopts the CQRS pattern, which segregates the read and write operations.

- Architecture
.Presentation Layer(API and Presentation folders): Contains the user interface, web components (for .NET Web Assembly), and API endpoints.
.Core Layer (Application, Domain): Implements the CQRS pattern to handle commands and queries, containing command handlers and query handlers.
.Domain Layer: Represents the core business logic of the application, including entities, value objects, and domain services.
.Infrastructure Layer(Identity, Persistence, Infrastructure): Provides implementations for external concerns like data access, repositories (using EF Core), logging, and external services.

- Technologies Used
.NET 7
.Entity Framework Core 7
.MediatR
.FluentValidation
.NET Web Assembly (blazor)
.AutoMapper
.Authenticaion via AspNetCore Identity
.xUnit Test

- Installation
. Clone and open solution via Visual Studio
. Set Api.Server as Start Project
. Open Nuget Package Console -> set Project is Infrastructure/Persistence -> run command (create database and data) :
	update-database -migration 20230726022346_InitalMigration -context EcommerceDbContext
. Then run another command (create fulltext indexes): 
	update-database -migration 20230726042544_MigrationToAddFullText -context EcommerceDbContext 

. Change project in Nuget package console to: Infrastructure/Identity -> run commnad (create Identity tables) :
	update-database -migration 20230727020312_InitialIdentityMigration -context IdentityDbContext

- Usage
 .Start Api.Server project
 .Then start EcommerceWeb project
	User login: user/123456 (role: User)
	Admin login: admin/123456 (rol: Administrator)