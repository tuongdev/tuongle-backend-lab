﻿using Domain.Common;

namespace Domain;

public class Product : BaseEntity
{
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Rating { get; set; }
    public double Price { get; set; }
    public Category Category { get; set; }
    public int CategoryId { get; set; }
    public int Remaining { get; set; }
}
