﻿using Domain.Common;

namespace Domain;

public class Order : BaseEntity
{
    public double TotalPrice { get; set; }
    public int Status { get; set; }
    public IEnumerable<OrderDetail>? OrderDetails { get; set; }
}
