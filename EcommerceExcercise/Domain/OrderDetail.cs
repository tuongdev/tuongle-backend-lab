﻿using Domain.Common;

namespace Domain;

public class OrderDetail : BaseEntity
{
    public int OrderId { get; set; }
    public Product Product { get; set; }
    public int ProductId { get; set; }
    public int Quantity { get; set; }
    public double Amount { get; set; }
}
